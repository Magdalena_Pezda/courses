/*
 * Student.h
 *
 *  Created on: 06.04.2017
 *      Author: Magda
 */

#ifndef STUDENT_H_
#define STUDENT_H_

#include "Person.h"

class Course;
const short maxStudentCourses=3;


class Student : public Person{
public:
	//constructors
	Student(string name,string surname,int age,string email);

	//destructors
	~Student();

	//atributes
	Course *studentCourses[maxStudentCourses];

	//methods
	void printPersonDescription();
	void printStudentCourses();
	void addCourseToStudent(Course* studentCourse);


	int studentCrsNumber;



};
#endif /* STUDENT_H_ */



