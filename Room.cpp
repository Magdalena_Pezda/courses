/*
 * Room.cpp
 *
 *  Created on: 01.04.2017
 *      Author: Magda
 */

#include "Room.h"
#include <iostream>
using namespace std;

//constructors
Room::Room() : salNumber(0), salCapacity(12), nameBuilding("A"), courseNumber(0) {
}

Room::Room(int mySalNumber, int mySalCapacity, string myNameBuilding) {
	this->salNumber=mySalNumber;
	this->salCapacity=mySalCapacity;
	this->nameBuilding=myNameBuilding;
	this->courseNumber=0;
}

void Room::roomDescription() {
	std:: cout << "Sal number: " << salNumber << ", capacity: " << salCapacity << ", building name: " << nameBuilding << std::endl;
}

void Room::addCourse(Course myCourse) {
		if(courseNumber<3) {
			courseList[courseNumber]=myCourse;
			courseNumber++;
		}
		else
			cout << "Too many courses" << endl;
}

void Room::printCoursesAtRoom() {
	for (int i=0; i<courseNumber; i++) {
		courseList[i].printCourseTopic();
	}
}

