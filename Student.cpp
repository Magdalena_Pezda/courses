/*
 * Student.cpp
 *
 *  Created on: 06.04.2017
 *      Author: Magda
 */

#include "Student.h"
#include "Person.h"
#include "Course.h"
#include "Room.h"

//constructors
Student::Student(string name,string surname,int age,string email):Person(name,surname,age,email){
	this->studentCrsNumber=0;
}

//destructors
Student::~Student(){
}

//methods
void Student::printPersonDescription() {
	Person::printPersonDescription();
}

void Student::addCourseToStudent(Course* studentCourse) {
	if (studentCrsNumber<maxStudentCourses){
		studentCourses[studentCrsNumber]=studentCourse;
		studentCrsNumber++;
		cout<<"Adding compete"<<endl;
	}
	else
		cout << "Maximum number of courses is 3. You can't add more courses";
}

void Student::printStudentCourses() {
	for (int i=0; i<studentCrsNumber; i++) {
		cout << "teeeest" << endl;
		studentCourses[i]->printCourseTopic();
	}
}







