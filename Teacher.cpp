/*
 * Teacher.cpp
 *
 *  Created on: 06.04.2017
 *      Author: Magda
 */

#include "Person.h"
#include "Teacher.h"
#include "Course.h"

Teacher::Teacher(string name, string surname, int age, string email): Person(name,surname,age,email){
	this->teacherCrsNumber=0;
}

Teacher::~Teacher() {
}

void Teacher:: printPersonDescription() {
	Person::printPersonDescription();
}

void Teacher::addCourseToTeacher(Course* teacherCourse) {
	if (teacherCrsNumber<maxTeacherCrsNumber){
		teacherCourses[teacherCrsNumber]=teacherCourse;
		teacherCrsNumber++;
	}
	else
		cout << "Sorry, no more courses to add";
}

void Teacher::printTeacherCourses() {
	for (int i=0; i<teacherCrsNumber; i++) {
		teacherCourses[i]->printCourseTopic();
	}
}


