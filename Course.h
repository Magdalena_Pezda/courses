/*
 * Course.h
 *
 *  Created on: 06.04.2017
 *      Author: Magda
 */

#ifndef COURSE_H_
#define COURSE_H_

#include "Teacher.h"
#include "Student.h"
#include <iostream>
using namespace std;

const int groupCapacity=3;
const int maxCourseTeachers=3;

class Room;
class Course {
public:
	Course();
	Course(string courseTopic, string courseDescription,string startDate, string endDate);
	~Course();

	//methods
	void printCourseTeachers();
	void printCourseStudents();
	void addStudentToCourse(Student* newStudent);
	void addTeacherToCourse(Teacher *newTeacher);
	void printCourseTopic();
	void printCourseDescriptions();

	//atributes
	Room *courseRoom[];
	string startDate;
	string endDate;
	Teacher *teacherCourses[]; //tablica wskaznikow zeby uniknac wykluczenia
	Student *courseStudents[groupCapacity]; //tablica wskaznikow zeby uniknac wykluczenia
	string courseTopic;
	string courseDescription;
	string newTeacher;

	int availableCapacity;
	int nbrCourseStudents;
	int nbrCourseTeachers;

};

#endif /* COURSE_H_ */
