/*
 * Room.h
 *
 *  Created on: 01.04.2017
 *      Author: Magda
 */

#ifndef ROOM_H_
#define ROOM_H_

#include "Course.h"
#include <string>

using namespace std;

class Room {
public:
	//constructors
	Room();
	Room(int mySalNumber, int mySalCapacity, string myNameBuilding);

	//methods
	void roomDescription();
	void addCourse(Course myCourse);
	void printCoursesAtRoom();


	// atributes
protected:
	int salNumber;
	int salCapacity;
	string nameBuilding;
	Course courseList[20];
	 int courseNumber;

};

#endif /* ROOM_H_ */
