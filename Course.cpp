/*
 * Course.cpp
 *
 *  Created on: 06.04.2017
 *      Author: Magda
 */

#include "Course.h"
#include <iostream>
using namespace std;


Course::Course() {
	this->courseTopic="Temat test";
	this->courseDescription="Przykladowy opis";
	this->availableCapacity=20;
	this->nbrCourseStudents=2;
	this->nbrCourseTeachers=5;
	this->newTeacher="Jan Kowalsky";
}


Course::Course(string courseTopic, string courseDescription, string startDate, string endDate) {
	this->courseTopic=courseTopic;
	this->availableCapacity=groupCapacity;
	this->courseDescription=courseDescription;
	this->startDate=startDate;
	this->endDate=endDate;
	this->nbrCourseStudents=0;
	this->nbrCourseTeachers=0;
	this->newTeacher=newTeacher;
}

Course::~Course(){
}

void Course::printCourseDescriptions(){
	cout << "COURSE: "<<courseTopic
		<<", DESCRIPTION: "<<courseDescription
		<<", START DATE: "<<startDate
		<<", END DATE: " <<endDate<<endl;
}

void Course::printCourseTopic() {
	cout << courseTopic << endl;
}

void Course::addStudentToCourse(Student* newStudent) {
	if (availableCapacity>0) {
		cout<<nbrCourseStudents<<endl;
		newStudent->printPersonName();
		cout << availableCapacity << "test: liczba dostepnych miejsc" << endl;
		courseStudents[nbrCourseStudents]=newStudent;//jezeli sa miejsca zapisz
		availableCapacity--;
		nbrCourseStudents++;
	}
	else
	cout << "No more places at this course!!";
}

void Course::printCourseStudents() {
	for (int i=0; i<nbrCourseStudents; i++){
		courseStudents[i]->printPersonName();
	}
}

void Course::addTeacherToCourse (Teacher *newTeacher) {
	if (nbrCourseTeachers<maxCourseTeachers) {
		teacherCourses[nbrCourseTeachers]=newTeacher;
		nbrCourseTeachers++;
	}
	else
		cout << "No more courses to add. Maximum course teachers=3" << endl;
}

void Course::printCourseTeachers() {
	for (int i=0; i<nbrCourseTeachers; i++){
		teacherCourses[i]->printPersonName();
	}
}


