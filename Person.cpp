/*
 * Person.cpp
 *
 *  Created on: 01.04.2017
 *      Author: Magda
 */

#include "Person.h"
#include <string>

//constructors
Person::Person (string name,string surname,int age,string email) {
	this-> name=name;
	this->surname=surname;
	this->age=age;
	this->email=email;
}

void Person::printPersonDescription() {
	cout << "name:" << name << ", " << "surname:" << surname << ", " << "age:" << age << ", " << "email:" << email << endl;
}

void Person::printPersonName() {
	cout << name << " " << surname << endl;
}

string Person::getName() {
	return name;
}

string Person::getSurname() {
	return surname;
}
