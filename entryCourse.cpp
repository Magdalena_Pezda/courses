//============================================================================
// Name        : entryCourse.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Room.h"
#include "Person.h"
#include "Student.h"
#include "Teacher.h"
#include "Course.h"

using namespace std;

void showMainMenu();
void showCoursesMenu();
void showSalMenu();

int nbrStudents=0;
Student *studentsTab[120];

int main() {
	//my changes
//courses rooms
Room sal1(1,25,"A");
Room sal2(2,25,"B");

//adding teachers
Teacher teacher1("Jan", "Kowalski", 42, "j.kowalski@sta.com");
Teacher teacher2("Jozef", "Stach", 32, "j.stach@sta.com");
Teacher teacher3("Aleksandra", "Sienkiewicz", 41, "a.sienkiewicz@sta.com");
Teacher teacher4("Jolanta", "Lukasik", 52, "j.lukasik@sta.com");

//adding courses
Course biology("Biology", "My description Biology", "01.10.2017", "12.02.2018");
Course chemistry("Chemistry", "My description Chemistry", "12.09.2017", "12.01.2018");
Course history("History", "My description History", "26.09.2017", "01.02.2018");
Course math("Mathematics", "My description Mathematics", "01.09.2017", "30.01.2018");
Course physics("Physics", "My description Physics", "10.09.2017", "10.01.2018");

//adding courses to room
sal1.addCourse(biology);
sal1.addCourse(chemistry);
sal1.addCourse(history);
sal2.addCourse(math);
sal2.addCourse(physics);

//adding courses teachers
teacher1.addCourseToTeacher(&biology);
teacher1.addCourseToTeacher(&chemistry);
teacher1.addCourseToTeacher(&physics);
teacher2.addCourseToTeacher(&history);
teacher3.addCourseToTeacher(&math);
teacher4.addCourseToTeacher(&biology);
teacher4.addCourseToTeacher(&math);

biology.addTeacherToCourse(&teacher1);
biology.addTeacherToCourse(&teacher4);
chemistry.addTeacherToCourse(&teacher1);
history.addTeacherToCourse(&teacher2);
math.addTeacherToCourse(&teacher3);
math.addTeacherToCourse(&teacher4);
physics.addTeacherToCourse(&teacher1);


//--------------------------------------------------------------------------------------------

cout<<"*        SCHOOL TUTORING ACADEMY         *"<<endl;
int mainOption;

do{
	showMainMenu();
	cin>>mainOption;
	if (mainOption==6){
		cout<<"Closing program. Thank you for your visit.";
		break;
	}
	switch (mainOption) {
		case 1: { //adding student to course
			showCoursesMenu();
			int courseOption;
			cin>>courseOption;
			if(courseOption==1){
				string studentName;
				string studentSurname;
				int studentAge;
				string studentEmail;
				cout<<"Please enter you name"<<endl;
				cin>>studentName;
				cout<<"Please enter you surname"<<endl;
				cin>>studentSurname;
				cout<<"Please enter age"<<endl;
				cin>>studentAge;
				cout<<"Please enter email"<<endl;
				cin>>studentEmail;
				Student *studentNew = new Student(studentName,studentSurname,studentAge,studentEmail);
				biology.addStudentToCourse(studentNew);
				studentNew->addCourseToStudent(&biology);

				studentsTab[nbrStudents]=studentNew; //adding student to students table
				nbrStudents++;
				break;
			}
			else if(courseOption==6){
				break;
			}
		break;
		}

		case 2: { //student courses
			string studName;
			string studSurname;
			cout<<"Please enter your name"<<endl;
			cin>>studName;
			cout<<"Please enter your surname"<<endl;
			cin>>studSurname;

			for(int i=0;i<=nbrStudents;i++){
				if (studentsTab[i]->getName()==studName){
					if (studentsTab[i]->getSurname()==studSurname){
						studentsTab[i]->printStudentCourses();
						break;
					}
					else {
						cout<<"No name at system"<<endl;
					}
				}
				else {
					cout<<"No name at system"<<endl;
				}
			}
			break;
		}

		case 3: { //printing courses at room
			int roomNumber;
			showSalMenu();
			cin>>roomNumber;
			if (roomNumber==1) {
				sal1.roomDescription();
				sal1.printCoursesAtRoom();
			}
			else if (roomNumber==2) {
				sal2.roomDescription();
				sal2.printCoursesAtRoom();
			}
			else if(roomNumber==3) {
				break;
			}
		break;
		}

		case 4: {//printing list course student
			showCoursesMenu();
			int nbrList;
			cin>>nbrList;
			if (nbrList==1) {
				biology.printCourseStudents();
				cout<<"Please enter to back to main menu"<<endl;
			}
			else if (nbrList==6) {
				break;
			}
		break;
		}

		case 5:
			showCoursesMenu();
			int nbrList2;
			cin>>nbrList2;
			if(nbrList2==1){
				biology.printCourseTeachers();
			}
			else if(nbrList2==2){
				chemistry.printCourseTeachers();
			}
			else if(nbrList2==3){
				history.printCourseTeachers();
			}
			else if(nbrList2==4){
				math.printCourseTeachers();
			}
			else if(nbrList2==5){
				physics.printCourseTeachers();
			}
			else if(nbrList2==6){
				break;
			}
	}
}
while(mainOption<6);
}

//--------------------------------------------------------------------------------------------

void showMainMenu() {
	cout<<"\nMAIN MENU:"<<"\n"
		<<"Select option and enter:"<<"\n"
		<< "1. Adding to course."<<"\n"
		<< "2. Student courses"<<"\n"
		<< "3. Print courses at room"<<"\n"
		<< "4. Print a list of students of chosen course"<<"\n"
		<< "5. Course teachers"<<"\n"
		<< "6. Exit"<<endl;
}

void showCoursesMenu() {
	cout<<"Please select course:\n"
		<<"1. Biology\n"
		<<"2. Chemistry\n"
		<<"3. History\n"
		<<"4. Mathematics\n"
		<<"5. Physics\n"
		<<"6. Back to main menu"<<endl;
}

void showSalMenu(){
	cout<<"Please select number of room\n"
	<<"1. Sal 1\n"
	<<"2. Sal 2\n"
	<<"3. Back to main menu\n";
}
