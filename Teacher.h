/*
 * Teacher.h
 *
 *  Created on: 06.04.2017
 *      Author: Magda
 */

#ifndef TEACHER_H_
#define TEACHER_H_

#include "Person.h"
// #include "Course.h" trzeba usunac naglowek aby nazwy sie nie wykluczaly
const short maxTeacherCrsNumber=3;


class Course; // dodane by klasy siebie nie wykluczaly nawzajem
class Teacher: public Person {
public:
	Teacher(string name, string surname, int age, string email);
	~Teacher();

	//atributes
	Course* teacherCourses[maxTeacherCrsNumber]; // wskaznik na tablice tak zeby klasy sie nie wykluczaly
	int teacherCrsNumber;


	//methods
	void printPersonDescription();
	void printTeacherCourses();
	void addCourseToTeacher(Course* teacherCourse);


};

#endif /* TEACHER_H_ */
