/*
 * Person.h
 *
 *  Created on: 01.04.2017
 *      Author: Magda
 */

#ifndef PERSON_H_
#define PERSON_H_

#include <iostream>
#include <string>

using namespace std;

class Person {
public:

//constructors
	Person(string name,string surname,int age,string email);

//methods
	void printPersonDescription();
	void printPersonName();
	string getName();
	string getSurname();
//do przetestowania funkcja bool ktora porowna imiona i nazwiska

//atrributes
protected:
	string name;
	string surname;
	int age;
	string email;

};

#endif /* PERSON_H_ */
